import { Injectable } from '@angular/core';
import {Card} from '../CardClass';
import { Observable, of } from 'rxjs';
import  {CreateNewPostFormComponent} from '../components/card-post/subComponents/create-new-post-form/create-new-post-form.component';
import {max} from 'rxjs/operators';



function randomInteger(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    rand = Math.floor(rand);
    return rand;
}
function x(mas: string[]) {
    let length = mas.length - 1;
    let random = randomInteger(0, length);
    return mas[random];
}

export class CardBaseService {
    get data() {
        return this._data;
    }

    // get dataTag() {
    //     return this._dataTag;
    // }

    setData(tag: string, img: string, title: string, brief: string) {
        this._data.unshift(new Card(tag, img, title, brief));
    }
    public _dataTag: string[] = ['A', 'B', 'C'];

    public _data: Card[] = [
        {tag: x(this._dataTag), img: 'img1.jpg', title: 'Заголовок А', brief: 'loremAloremAloremAloН remA123123123'},
        {tag: x(this._dataTag), img: 'img2.jpg', title: 'Заголовок B', brief: 'loremAloremAlore mAloremA'},
        {tag: x(this._dataTag), img: 'img3.jpg', title: 'Заголовок C', brief: 'loremAloremAlore mAloremA'},
        {tag: x(this._dataTag), img: 'img1.jpg', title: 'Заголовок А', brief: 'loremAloremAloremAloН remA123123123'},
        {tag: x(this._dataTag), img: 'img2.jpg', title: 'Заголовок B', brief: 'loremAloremAlore mAloremA'},
        {tag: x(this._dataTag), img: 'img1.jpg', title: 'Заголовок А', brief: 'loremAloremAloremAloН remA123123123'},
        {tag: x(this._dataTag), img: 'img2.jpg', title: 'Заголовок B', brief: 'loremAloremAlore mAloremA'},
        {tag: x(this._dataTag), img: 'img1.jpg', title: 'Заголовок А', brief: 'loremAloremAloremAloН remA123123123'},
        {tag: x(this._dataTag), img: 'img2.jpg', title: 'Заголовок B', brief: 'loremAloremAlore mAloremA'},
        {tag: x(this._dataTag), img: 'img1.jpg', title: 'Заголовок А', brief: 'loremAloremAloremAloН remA123123123'},
        {tag: x(this._dataTag), img: 'img2.jpg', title: 'Заголовок B', brief: 'loremAloremAlore mAloremA'},
        {tag: x(this._dataTag), img: 'img1.jpg', title: 'Заголовок А', brief: 'loremAloremAloremAloН remA123123123'},
        {tag: x(this._dataTag), img: 'img2.jpg', title: 'Заголовок B', brief: 'loremAloremAlore mAloremA'},
    ];

    // getData(): Card[]{
    //     return data;
    // }
    //
    // addData(tag:string, img:string, title:string, brief:string){
    //     this._data.push(new Card(tag, img, title, brief));
    // }
    @Injectable({
        providedIn: 'root'
    })
constructor() { }
}
