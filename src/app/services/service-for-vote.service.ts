import { Injectable } from '@angular/core';
import {QuestionNature} from '../question-nature';
@Injectable({
  providedIn: 'root'
})
export class ServiceForVoteService {

  quest1: QuestionNature ={
    question: 'Вы считаете, что ГМО вреден?',
    answers: ['он безусловно вреден!', 'его вредность не подтверждена', 'моя хата с краю от этой темы'],
    variant: 1,
    answered: false
  };
  quest2: QuestionNature ={
    question: 'у паука 8 лап, а у рака?',
    answers: ['4!', '8', '18'],
    variant: 2,
    answered: false
  };
  quest3: QuestionNature ={
    question: 'Самый большой город в мире?',
    answers: ['Токио', 'Москва', 'Шанхай', 'Джакарта'],
    variant: 0,
    answered: false
  };
  quest4: QuestionNature ={
    question: 'Что является фруктом??',
    answers: ['Помидор', 'Патиссон', 'Виноград', 'Банан'],
    variant: 0,
    answered: false
  };
  quest5: QuestionNature ={
    question: 'Кто сказал: «Мыслю, следовательно, существую»?',
    answers: ['Фридрих Ницше', 'Сенека', 'Артурчик Шопенгауэр', 'Рене Декарт'],
    variant: 3,
    answered: false
  };
  public _data = [this.quest1, this.quest2, this.quest3, this.quest4, this.quest5];
  get data()
  {
    return this._data;
  }

  constructor() { }
}
