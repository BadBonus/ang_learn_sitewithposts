export class QuestionNature {
    constructor(
        public question: string,
        public answers: string [],
        public variant: number,
        public  answered: boolean) {}
  }

