import { Component, OnInit } from '@angular/core';
import {Card} from '../../CardClass';
import {CardBaseService} from '../../services/card-base.service';

@Component({
  selector: 'app-card-post',
  templateUrl: './card-post.component.html',
  styleUrls: ['./card-post.component.css']
})
export class CardPostComponent implements OnInit {
  // POSTS: Card[] = [
  //     {id:0, tag:'A', img:"img1.jpg", title:"Заголовок А", brief: "loremAloremAloremAloН remA123123123"},
  //     {id:1, tag:'B', img:"img2.jpg", title:"Заголовок B", brief: "loremAloremAlore mAloremA"},
  //     {id:2, tag:'C', img:"img3.jpg", title:"Заголовок C", brief: "loremAloremAlore mAloremA"},
  //     {id:0, tag:'A', img:"img1.jpg", title:"Заголовок А", brief: "loremAloremAloremAloН remA123123123"},
  //     {id:1, tag:'B', img:"img2.jpg", title:"Заголовок B", brief: "loremAloremAlore mAloremA"},
  //     {id:0, tag:'B', img:"img1.jpg", title:"Заголовок А", brief: "loremAloremAloremAloН remA123123123"},
  //     {id:1, tag:'B', img:"img2.jpg", title:"Заголовок B", brief: "loremAloremAlore mAloremA"},
  //     {id:0, tag:'C', img:"img1.jpg", title:"Заголовок А", brief: "loremAloremAloremAloН remA123123123"},
  //     {id:1, tag:'C', img:"img2.jpg", title:"Заголовок B", brief: "loremAloremAlore mAloremA"},
  //     {id:0, tag:'C', img:"img1.jpg", title:"Заголовок А", brief: "loremAloremAloremAloН remA123123123"},
  //     {id:1, tag:'B', img:"img2.jpg", title:"Заголовок B", brief: "loremAloremAlore mAloremA"},
  //     {id:0, tag:'A', img:"img1.jpg", title:"Заголовок А", brief: "loremAloremAloremAloН remA123123123"},
  //     {id:1, tag:'C', img:"img2.jpg", title:"Заголовок B", brief: "loremAloremAlore mAloremA"},
  // ];


    filter: string = 'all';
    showGreeting: boolean = true;

    Posts: any;

    HiddenOfOn(global: string, local: string)
   {
      if (global == local)
      {
          return true;
      }
      else if(global == "all")
      {
          return true;
      }
      else
       {
           return false;
       }
   }


    constructor(
        public CardBaseService: CardBaseService
    ) {

    }
 getPosts():void{

 }
  ngOnInit() {
      this.Posts = this.CardBaseService.data;
  }

}
