import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {ServiceForVoteService} from '../../../../services/service-for-vote.service';

@Component({
  selector: 'app-create-new-form-reactive',
  templateUrl: './create-new-form-reactive.component.html',
  styleUrls: ['./create-new-form-reactive.component.css']
})
export class CreateNewFormReactiveComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
