import { Component, OnInit } from '@angular/core';
import {CardBaseService} from '../../../../services/card-base.service';

@Component({
  selector: 'app-create-new-post-form',
  templateUrl: './create-new-post-form.component.html',
  styleUrls: ['./create-new-post-form.component.css'],
})
export class CreateNewPostFormComponent implements OnInit {

  constructor(private cardBaseService: CardBaseService) {}

  tag: string;
  title: string;
  brief: string;
  hiddenForm = true;
  allTags = this.cardBaseService._dataTag;
  newTagFormVisible = false;
  hideForm() {
    this.hiddenForm = ! this.hiddenForm;
    return false;
  }

  addItem() {
    this.cardBaseService.setData(this.tag, 'img_newPost.jpg', this.title, this.brief);
    this.hideForm();
  }

    newTagFormVisible_open()
    {
        this.newTagFormVisible = true;
        alert("hohoh");
    }

    newTagFormVisible_close()
    {
        this.newTagFormVisible = false;

    }

  ngOnInit() {
  }

}
