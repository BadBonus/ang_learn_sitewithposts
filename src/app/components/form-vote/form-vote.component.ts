import {Component, OnInit} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {CardBaseService} from '../../services/card-base.service';
import {QuestionNature} from '../../question-nature';
import {ServiceForVoteService} from '../../services/service-for-vote.service';

@Component({
    selector: 'app-form-vote',
    templateUrl: './form-vote.component.html',
    styleUrls: ['./form-vote.component.css'],

})
export class FormVoteComponent implements OnInit {

    massiveData: any;
    userAnswer: number;
    currentAnswers = 0;
    currentPos = 0;
    finish = false;
    callVote = false;
    takeAnswer(answer) {
        this.userAnswer = answer;
    }
    x(x)
    {
        console.log(x);
    }
    submit(currentQuestion) {
        this.currentPos = this.currentPos + 1;
        if (this.userAnswer == +(this.massiveData[currentQuestion].variant)) {
            this.currentAnswers = this.currentAnswers + 1;
        }
        if (this.currentPos == 5)
        {
            this.finish = !this.finish;
        }
        this.userAnswer = undefined;
    }


    constructor(public serviceForVoteService: ServiceForVoteService) {
    }

    ngOnInit() {
        this.massiveData = this.serviceForVoteService.data;
    }

}
