export class Card {
    constructor(
    public tag: string,
    public img: string,
    public title: string,
    public brief: string) {}
}

