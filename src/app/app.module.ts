import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms'
import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CardPostComponent } from './components/card-post/card-post.component';
import { CreateNewPostFormComponent } from './components/card-post/subComponents/create-new-post-form/create-new-post-form.component';
import {CardBaseService} from './services/card-base.service';
import { FormVoteComponent } from './components/form-vote/form-vote.component';
import {ServiceForVoteService} from './services/service-for-vote.service';
import { CreateNewFormReactiveComponent } from './components/card-post/subComponents/create-new-form-reactive/create-new-form-reactive.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    CardPostComponent,
    CreateNewPostFormComponent,
    FormVoteComponent,
    CreateNewFormReactiveComponent,
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule

  ],
  providers: [CardBaseService, ServiceForVoteService],
  bootstrap: [AppComponent]
})
export class AppModule { }
